let leftArrow = document.querySelector('#leftarrow')
let rightArrow = document.querySelector('#rightarrow')
let slides = document.getElementsByClassName('slide')


rightArrow.addEventListener('click', () => {
    for (let i = 0; i < slides.length; i++) {
        if (slides[i].classList.contains('active')) {
            slides[i].classList.remove('active')
            if (i + 1 === slides.length) {
                slides[0].classList.add('active')
            } else {
                slides[i + 1].classList.add('active')
            }
            break;
        }

    }
})



leftArrow.addEventListener('click', () => {
    for (let i = 0; i < slides.length; i++) {
        if (slides[i].classList.contains('active')) {
            slides[i].classList.remove('active')
            if (i - 1 < 0) {
                slides[slides.length - 1].classList.add('active')
            } else {
                slides[i - 1].classList.add('active')
            }
            break;
        }

    }
})