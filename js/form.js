let input = document.querySelector('#firstname')
let submit = document.querySelector('#submitbutton')
let comment = document.querySelector('#comment')


submit.addEventListener('click', () => {
    if (input.value === '' || comment === '' ) {
        alert('You did not enter information in all fields. Try again.')
        return false
    } else {
        alert('Your comment has been submitted. Thank you!')
        return true
    }
});